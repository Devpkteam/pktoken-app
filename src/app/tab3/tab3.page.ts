import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
//import { UsuarioService } from '../services/usuario.service';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';
import { AlertController } from '@ionic/angular';
import { HttpClient } from '@angular/common/http';
import { URL_SERVICIOS } from '../config/config';
import { PktokenService } from '../services/pktoken/pktoken.service';

@Component({
  selector: 'app-tab3',
  templateUrl: 'tab3.page.html',
  styleUrls: ['tab3.page.scss']
})
export class Tab3Page implements OnInit {

  form: FormGroup;
  nombre:string;
  email:string;
  id:string;
  img: string;
  password;
  rpassword;


  nombreCtrl: FormControl;
  emailCtrl: FormControl;
  passwordCtrl: FormControl;

  constructor(public pktokenService:PktokenService,private camera: Camera,private fb: FormBuilder,public alertController: AlertController,private http:HttpClient/*public usuarioService: UsuarioService*/) {}

  ngOnInit() {
    this.tokeID = localStorage.getItem("token")
    this.id = localStorage.getItem('id');
    this.nombre = localStorage.getItem('nombre');
    this.email = localStorage.getItem('email');

    this.nombreCtrl = new FormControl(this.nombre, [Validators.required, Validators.minLength(2)]);
    this.emailCtrl = new FormControl(this.email);
    this.passwordCtrl = new FormControl('',[Validators.required])
    
    this.form = this.fb.group({
      usuario: this.nombreCtrl,
      password:this.passwordCtrl
    });
  }

  tokeID;

  tokenIdReciver;
  getToken(){
    this.tokeID = localStorage.getItem("token");
  }
  actualizarInfo(){
    this.http.put(`${URL_SERVICIOS}/usuario/${localStorage.getItem('id')}`,this.form.value).subscribe((msg:any)=>{
  
      if(msg.ok){
        this.alertActualizado()
      }
    })
  }


  
 
  

  async alertActualizado() {
    const alert = await this.alertController.create({
      header: 'Ok',
      subHeader: 'Los datos se actualizaron correctamente!',
      message: '',
      buttons: ['Aceptar']
    });

    await alert.present();
  }

  async presentAlertConfirm() {
    const alert = await this.alertController.create({
      header: 'Actualizar',
      message: 'Seguro que desea actualizar tu informacion?',
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            console.log('Confirm Cancel: blah');
          }
        }, {
          text: 'Aceptar',
          handler: () => {
            this.actualizarInfo()
          }
        }
      ]
    });

    await alert.present();
  }


  camara() {
    const options: CameraOptions = {
      quality: 70,
      sourceType: this.camera.PictureSourceType.CAMERA,
      destinationType: this.camera.DestinationType.NATIVE_URI,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE ,
      correctOrientation : false
      // targetWidth: 200,
      // targetHeight: 200,
    }

    
    this.camera.getPicture(options).then((imageData) => {
      console.log(imageData);
      
     // imageData is either a base64 encoded string or a file URI
     // If it's base64 (DATA_URL):
     let base64Image = 'data:image/jpeg;base64,' + imageData;
     this.img = base64Image;
     console.log('¿¿¿¿¿¿', base64Image);
     
    }, (err) => {
     // Handle error
    });
  }


  onSubmit() {
    // this._usuarioService.buscarUsuarios(this.id);
  }


}
