import { Component, OnInit } from '@angular/core';
import { Router } from  "@angular/router";
import { LoginServiceService } from '../services/login-service.service';
import { AlertController } from '@ionic/angular';
import { Socket } from 'ngx-socket-io';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  constructor(
    private socket: Socket,
    private  _loginService:  LoginServiceService, 
    private  router:  Router,
    public alertController: AlertController) { }

  ngOnInit() {
    this.socket.connect();
  }

  async datosInvalidos() {
    const alert = await this.alertController.create({
      header: 'Error',
      subHeader: 'No puede ingresar',
      message: 'Los datos no coinciden.',
      buttons: ['OK']
    });

    await alert.present();
  }


  login(form){
    this._loginService.login(form.value).subscribe((res)=>{
      localStorage.setItem('id', res.usuario._id);
      localStorage.setItem('email', res.usuario.email + '');
      localStorage.setItem('nombre', res.usuario.nombre);
      this.router.navigateByUrl('t');
    }, ()=>{
      this.datosInvalidos();
    });
  }
}
