import { Component, OnInit } from '@angular/core';
import { InventarioService } from '../services/inventario.service';

@Component({
  selector: 'app-tab5',
  templateUrl: './tab5.pages.html',
  styleUrls: ['./tab5.pages.scss'],
})
export class Tab5Page implements OnInit {


  inventario
  desde: number = 0;
  cargando: boolean = true;
  totalRegistros: number = 0; 
  constructor(public _inventarioService: InventarioService, ) { }
  ngOnInit() {
    this.cargarInventario();
  }

  cargarInventario() {
    //  console.log(this.inventario);
      this.cargando = true;
      this._inventarioService.cargarInventario( this.desde )
      .subscribe( (resp: any) => {
        this.ngOnInit();
        this.totalRegistros = resp.total;
        this.inventario = resp.inventario;
        this.cargando = false;
      //  console.log(this.inventario);
  
      })
    }
}
