import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BarcodeScanner } from '@ionic-native/barcode-scanner/ngx';
import { HttpClientModule } from  '@angular/common/http';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { FCM } from '@ionic-native/fcm/ngx';
import { AngularFireModule } from 'angularfire2';
import { AngularFireDatabaseModule } from 'angularfire2/database';

import { URL_SERVICIOS } from 'src/app/config/config';


import { SocketIoModule, SocketIoConfig } from 'ngx-socket-io';
const config: SocketIoConfig = { url: URL_SERVICIOS, options: {} };

const firebaseConfig = {
  apiKey: "AIzaSyDIvg8u_QwFwZdzNJ5flrvUdywuC82kqoo",
  authDomain: "pkcoinapp.firebaseapp.com",
  databaseURL: "https://pkcoinapp.firebaseio.com",
  projectId: "pkcoinapp",
  storageBucket: "pkcoinapp.appspot.com",
  messagingSenderId: "130682561580"
};

@NgModule({
  declarations: [AppComponent],
  entryComponents: [],
  imports: [BrowserModule, IonicModule.forRoot(), 
    AppRoutingModule,HttpClientModule, NoopAnimationsModule, SocketIoModule.forRoot(config),
    AngularFireModule.initializeApp(firebaseConfig,'pkcoinapp'),
    AngularFireDatabaseModule],
  providers: [
    StatusBar,
    SplashScreen,
    BarcodeScanner,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy },
    Camera,
    FCM
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
