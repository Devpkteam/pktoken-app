import { Component } from '@angular/core';
import { PktokenService } from '../services/pktoken/pktoken.service';

@Component({
  selector: 'app-tabs',
  templateUrl: 'tabs.page.html',
  styleUrls: ['tabs.page.scss']
})
export class TabsPage {

  constructor(public pktokenService:PktokenService) {
    this.updateToken()
  }


  updateToken(){
    this.pktokenService.updateTokenFmc(localStorage.getItem("token"),localStorage.getItem("id")).subscribe((res)=>{
        console.log(res)
    })
  }

}
