import { Injectable } from '@angular/core';
import { Inventario } from 'src/app/models/inventario.model';
import { HttpClient } from '@angular/common/http';
import { URL_SERVICIOS } from 'src/app/config/config';
import { map } from 'rxjs/operators'
import { catchError } from 'rxjs/operators'
import { Router } from '@angular/router';
//import { SubirArchivoService } from '../subir-archivos/subir-archivo.service';
import { throwError } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class InventarioService {

  inventario: Inventario;
  token: string;

  constructor(
    public http: HttpClient,
    public router: Router
    //public _subirArchivoService: SubirArchivoService
  ) {
    console.log('servicio de inventario listo')
  }

  crearInventario( inventario: Inventario ) {
    let url = URL_SERVICIOS +'/inventario';
    return this.http.post(url, inventario)
    .pipe(map( (resp:any) => {
        //swal('Inventario creado: ', resp.inventario.nombre, 'success');
        return resp.inventario;
    }),
    catchError( err => {
      console.log( err )
      //swal('Error' ,`${err.error.errors.errors.nombre.message}` ,'error' )
      return throwError(err)
    }));
  }

  
  cargarInventario( desde: number = 0 ){
    let url = URL_SERVICIOS + '/inventario?desde=' + desde;
    return this.http.get( url );
  }
  
  actualizarInventario( inventario: Inventario, token ) {
    console.log(inventario);
    this.token = token;
    let url = URL_SERVICIOS + '/inventario/' + inventario._id;
    url += '?token=' + this.token;
    console.log(url);
    return this.http.put( url, inventario )
      .pipe(map( ( resp: any ) => {
          let inventarioDB: Inventario = resp.inventario;
          console.log(inventarioDB)
        //swal('Producto actualizado: ', resp.inventario.nombre, 'success');
        return true;
      }))
  }

  // buscarUsuarios ( termino: string ) {
  //   let url = URL_SERVICIOS + '/busqueda/coleccion/usuarios/' + termino;
  //   return this.http.get( url )
  //     .pipe( map ( (resp : any) => resp.usuarios));
  // }

  borrarProducto (id: string) {
    let url = URL_SERVICIOS + '/inventario/' + id;
    url += '?token=' + this.token;
    return this.http.delete( url )
      .pipe(map( resp => {
        //swal("Producto borrado","el producto ha sido eliminado correctamente","success");
        return true;
      }));
  }
}
