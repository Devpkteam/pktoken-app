/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { PktokenService } from './pktoken.service';

describe('Service: Pktoken', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [PktokenService]
    });
  });

  it('should ...', inject([PktokenService], (service: PktokenService) => {
    expect(service).toBeTruthy();
  }));
});
