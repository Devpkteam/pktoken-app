import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { URL_SERVICIOS } from 'src/app/config/config';

import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class PktokenService {

  

constructor(public http:HttpClient) { }

getSolicitudes(duenioActual){
  let url = URL_SERVICIOS +'/solicitudes/' + duenioActual ;
  return this.http.get( url )
    .pipe(map( (resp:any) => {
      return resp;
    }));
  }

  deleteSolicitud(solicitud){
    let url = URL_SERVICIOS +'/solicitudes/' + solicitud ;
    return this.http.delete( url )
      .pipe(map( (resp:any) => {
        return resp;
      }));
    }

createRequest(solicitante, pktoken){
  return this.http.post(`${URL_SERVICIOS}/solicitudes`,{solicitante:solicitante, _idPkToken:pktoken });
}

updateOwnerPkoin(id,newOwner){
  return this.http.put(`${URL_SERVICIOS}/pktoken/${id}`,{nuevoDuenio:newOwner});
}

getPktokens(owner){
  let url = URL_SERVICIOS +'/pktoken/duenio/' + owner ;
  return this.http.get( url )
    .pipe(map( (resp:any) => {
      return resp;
    }));
  }

updateTokenFmc(token,id){
    return this.http.post(`${URL_SERVICIOS}/usuario/tokenfcm/${id}`,{token:token});
}

sendNotificationToken(id,nombre){
  return this.http.post(`${URL_SERVICIOS}/usuario/tokenfmc/notification/${id}`,{usuario:nombre})
}
  
}
