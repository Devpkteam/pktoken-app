import { Injectable } from '@angular/core';
import { Usuario } from 'src/app/models/usuario.model';
import { HttpClient } from '@angular/common/http';
import { URL_SERVICIOS } from 'src/app/config/config';
import { map } from 'rxjs/operators'
import { catchError } from 'rxjs/operators'
import { Router } from '@angular/router';
import { throwError } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class LoginServiceService {
  usuario: Usuario;

  constructor(
    public http: HttpClient,
    public router: Router,) { }

  login(usuario:Usuario){
    let url = URL_SERVICIOS +'/login';
    return this.http.post( url, usuario )
    .pipe(map( (resp:any) => {
      return resp;
    }),
    catchError( err => {
      return throwError(err)
    }));
  }

}
