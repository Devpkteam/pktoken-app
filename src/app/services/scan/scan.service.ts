import { Injectable } from '@angular/core';
import { HTTP } from '@ionic-native/http/ngx';
import { BarcodeScanner } from '@ionic-native/barcode-scanner/ngx';
import { URL_SERVICIOS } from '../../config/config';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ScanService {

  constructor(
    private barcodeScanner: BarcodeScanner,
    private http: HttpClient
  ) { }

  actualizar( token, id ) {
    let url = URL_SERVICIOS + '/pktoken/'+token._id;
    console.log(typeof(url));
    console.log(typeof(token));
    console.log(typeof(id));
    console.log(url, id);
    
    this.http.put(url, id)
      .toPromise()
      .then(resp=>{
        console.log(resp);
        console.log("===============****");
      });
  }

  // actualizar( token ) {
  //   console.log(token);
  //   let url = URL_SERVICIOS + '/pktoken';
  //   console.log(url);
  //   this.http.get(`http://localhost:3000/pktoken`)
  //     .toPromise()
  //     .then(resp=>{
  //       console.log(resp);
  //     });
  // }
}
