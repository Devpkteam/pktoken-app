import { Component } from '@angular/core';
import { BarcodeScanner } from '@ionic-native/barcode-scanner/ngx';
import { ScanService } from '../services/scan/scan.service';
import { PktokenService } from '../services/pktoken/pktoken.service';

@Component({
  selector: 'app-tab2',
  templateUrl: 'tab2.page.html',
  styleUrls: ['tab2.page.scss']
})
export class Tab2Page {
  swiperOpts = {
    allowSlidePrev: false,
    allowSlideNext: false
  }
  
  constructor(
    private barcodeScanner: BarcodeScanner,
    public _scanService: ScanService,
    public pktokenService:PktokenService
  ) {}

  ionViewWillEnter() {
    this.scan();
  }
/*
  scan(){
    let barcodeData = {"_id":"8ee4b1239a3429738ee6076c","monto":"5"}
    // this._scanService.actualizar(barcodeData, {duenioActual:"6da2985a4e74f4208499e780"})
    this._scanService.actualizar(barcodeData)
      // console.log(resp);
  }*/

  scan() {
   
    let id_newOwner =  localStorage.getItem("id");
    
    this.barcodeScanner.scan().then(async barcodeData => {
      console.log(barcodeData)
      if(barcodeData.format == 'QR_CODE' && barcodeData.cancelled == false){
         let pkdata = barcodeData.text 
         if(pkdata && pkdata){
           console.log(pkdata)
           console.log(id_newOwner)
       this.pktokenService.createRequest(id_newOwner,pkdata).subscribe((msg:any)=>{
         alert(msg.msj)
       })
         }else{
           console.log('El codigo QR tiene un formato incorrecto')
         }
       
      }else{
        console.log('ha ocurrido un error')
      }
      
      
     }).catch(err => {
         console.log('Error', err);
     });
  }
}
