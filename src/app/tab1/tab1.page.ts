import { Component } from '@angular/core';

import { PktokenService } from '../services/pktoken/pktoken.service';

@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss']
})
export class Tab1Page {

  owner = localStorage.getItem('id');
  total;
  pktokens;

  constructor(
   public _pktokenService: PktokenService,
  ) {
    this.getpkoins();
    
  }

  ionViewWillEnter(){
    this.getpkoins();
  }

  getpkoins(){
    this._pktokenService.getPktokens(this.owner).subscribe(data=>{
      this.pktokens = data.usuarios;
      this.total = 0;
      console.log(this.pktokens[0])
      for(let i=0; i<this.pktokens.length; i++){
        this.total = (parseInt(this.total))  + (parseInt(this.pktokens[i].monto));
        console.log(this.total)
      }
      
    })
  }

}
