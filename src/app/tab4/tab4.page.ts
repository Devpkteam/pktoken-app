import { Component, OnInit } from '@angular/core';
import { PktokenService } from '../services/pktoken/pktoken.service';

@Component({
  selector: 'app-tab4',
  templateUrl: './tab4.page.html',
  styleUrls: ['./tab4.page.scss'],
})
export class Tab4Page implements OnInit {
  misSolicitudes;

  constructor(public pktokenService:PktokenService) { 
    this.do();
   }

   ionViewWillEnter() {
    this.do();
  }

  do(){
    this.pktokenService.getSolicitudes(localStorage.getItem('id')).subscribe(data => {
      this.misSolicitudes = data.pksolicitudes;
      console.log(this.misSolicitudes)
    });
  }

  ngOnInit() {
  }

  acepto(idSolicitante, pkoin, id){
    console.log(idSolicitante)
    console.log(pkoin)
    this.pktokenService.updateOwnerPkoin(pkoin, idSolicitante).subscribe(data => {
      console.log(data)
    });

    this.pktokenService.deleteSolicitud(id).subscribe(data => {
      console.log(data)
      this.misSolicitudes = [];
      this.do()
    });

    
  }

  rechazo(id){
    this.pktokenService.deleteSolicitud(id).subscribe(data => {
      console.log(data)
      this.misSolicitudes = [];
      this.do()
    });
  }

}
